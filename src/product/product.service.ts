import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class productService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product: Product = new Product();

    return this.productRepository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.productRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.productRepository.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateProductDto) {
    const product = await this.productRepository.findOneBy({ id: id });
    const updatedUser = { ...product, ...updateUserDto };

    return this.productRepository.save(updatedUser);
  }

  async remove(id: number) {
    const user = await this.productRepository.findOneBy({ id: id });

    return this.productRepository.remove(user);
  }
}
